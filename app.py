#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec  4 16:46:39 2019

@author: Marcin Szarejko
"""

import tkinter as tk
import mongo as mg

from tkinter import font
from tkinter.messagebox import showinfo


class App(tk.Tk):
    def __init__(self, *args, **kwargs):
        # initialize the window
        tk.Tk.__init__(self, *args, **kwargs)
        
        # set the window size
        self.geometry('350x320')
        
        # set the look of the frame
        self.main = tk.Frame(self)
        self.main.pack(side = tk.TOP, fill = 'both', expand = 1)
        self.main.grid_columnconfigure(0, weight = 1)
        self.main.grid_columnconfigure(1, weight = 0)
        self.main.grid_columnconfigure(2, weight = 1)
        
        # set title
        self.title('MongoDB')
        
        # set font
        self.font = font.Font(family = 'Helvetica', size = 18, weight = 'bold', slant = 'italic')
        
        # show contents
        self.entries = self.fill_out()
        self.buttons()
        
    def fill_out(self):
        # set labels
        tk.Label(self.main, text = 'Fill ins', font = self.font).grid(row = 0, columnspan = 3, pady = 20)
        tk.Label(self.main, text = 'Data names', font = self.font).grid(row = 1, column = 0, sticky = 'e', padx = 10)
        tk.Label(self.main, text = 'Values', font = self.font).grid(row = 1, column = 2, sticky = 'w', padx = 35)
        
        # initialize entries and store them in a list
        entries = []
        for i in range(10):
            ent1 = tk.Entry(self.main)
            ent1.grid(row = i+2, column = 0, sticky = 'e')
            tk.Label(self.main, text = '-->').grid(row = i+2, column = 1)
            ent2 = tk.Entry(self.main)
            ent2.grid(row = i+2, column = 2, sticky = 'w')
            entries.append([ent1, ent2])
        
        return entries
    
    def buttons(self):
        # display buttons 
        tk.Button(self.main, text = 'Generate data', command = lambda: self.generate()).grid(row = 12, column = 0, sticky = 'e', padx = 15, pady = 20)
        tk.Button(self.main, text = 'Insert data', command = lambda: self.insert_entries()).grid(row = 12, column = 2, sticky = 'w', padx = 15, pady = 20)
        
    def insert_entries(self):
        # retrieve entries and store them in a dictionary
        values = {str(name.get()): value.get() for name, value in self.entries if name.get() != '' and value.get() != ''}
        for ent1, ent2 in self.entries:
            ent1.delete(0, tk.END)
            ent2.delete(0, tk.END)
        # check if values are non empty
        if not values:
            showinfo(title = 'Error', message = 'Give at least one name and value to insert.')
        else:
            # insert entries
            mg.insert(values)
            # show info 
            showinfo(title = 'Data insertion', message = 'Data inserted successfully.')
        
    def generate(self):
        values = mg.generate_data()
        message = ['Generated data inserted.']
        for key, value in values.items():
            message.append(key + ': ' + str(value))
        showinfo(title = 'Data insertion', message = '\n'.join(message))
        
        
if __name__ == '__main__':
    app = App()
    app.mainloop()