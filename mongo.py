#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec  5 12:13:42 2019

@author: Marcin Szarejko
"""

import datetime

from random import randrange
from pymongo import MongoClient

default_host_port = 'mongodb://localhost:27017/'


def connect():
    # establish a connections to a database
    client = MongoClient(default_host_port)
    database = client['people']
    collection = database['stats']  

    return collection

def insert(values):
    # add time of insertion
    values['Date'] = str(datetime.datetime.utcnow())
    collection = connect()
    # insert
    collection.insert_one(values)
    
def find(by_variable = None, by_field = None):
    collection = connect()
    return collection.find(by_variable, by_field)
    
def generate_data():
    # example data for people with possible stats
    ex_stats = {'Weight': (40, 201), 'Height': (50, 251), 'BMI': None, 'Age': (0, 100), 'Runner': (0, 2)}
    values = {}
    # randomly generate values
    for key, value in ex_stats.items():
        if key == 'BMI':
            values[key] = values['Weight']/((values['Height']/100)**2)
        else:
            values[key] = randrange(*value)
    # insert into database
    insert(values)
    return values